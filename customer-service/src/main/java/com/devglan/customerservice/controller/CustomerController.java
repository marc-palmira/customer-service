package com.devglan.customerservice.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devglan.commons.Customer;
import com.devglan.commons.DataStore;
import com.devglan.commons.Product;
import com.devglan.customerservice.dto.CustomerDto;
import com.devglan.customerservice.exception.RecordNotFoundException;
import com.devglan.customerservice.service.CustomerService;


@RestController
@RequestMapping("/customers")
public class CustomerController {
    
    @Autowired
    private CustomerService customerService;

    @GetMapping("/{id}")
    public CustomerDto getCustomerById(@PathVariable String id) throws RecordNotFoundException{

    	List<Product> products = customerService.test(id);
//        List<Product> products = productClient.listProductsByCustomerId(id);
    	Customer customer = null;
    	try {
    		customer = DataStore.listCustomers().stream().filter(cust -> cust.getId().equalsIgnoreCase(id)).findFirst().get();
    	} catch (Exception e) {
    		throw new RecordNotFoundException("No Customer record exist for given id", id);
    	}
        CustomerDto dto = new CustomerDto();
        BeanUtils.copyProperties(customer, dto);
        dto.setProducts(products);
    	
        //Product pr1 = productClient.getProductById("PRD1");
        //Product pr2 = productClient.create(products.get(0));
        //List<Product> pr3 = productClient.listProducts();
        return dto;
    }
    
}
