package com.devglan.customerservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devglan.commons.Product;
import com.devglan.customerservice.feign.client.ProductClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class CustomerService {
	
//	@Autowired
//    private EurekaClient eurekaClient;
	
	@Autowired
	private ProductClient productClient;

    @HystrixCommand(fallbackMethod="failed")
//    public ProductList test(String id){
  public List<Product> test(String id){
//      InstanceInfo instanceInfo = eurekaClient.getNextServerFromEureka("product-service", false);
//      String serviceBaseUrl = instanceInfo.getHomePageUrl();
//		ProductList products = new RestTemplate().getForObject(serviceBaseUrl + "/products/customer/" + id, ProductList.class);

    List<Product> products = productClient.listProductsByCustomerId(id);
		
        return products;
    }

    public List<Product> failed(String id){
        return new ArrayList<Product>();
    }
}
