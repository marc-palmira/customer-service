package com.devglan.customerservice.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.devglan.commons.Product;
import com.devglan.customerservice.feign.config.CustomFeignConfig;

import feign.RequestLine;

//@FeignClient(name="product-service", configuration = CustomFeignConfig.class)
@FeignClient(name="product-service", decode404 = true)

public interface ProductClient {

	@GetMapping("/products")
    List<Product> listProducts();

	@GetMapping("/products/{id}")
    Product getProductById(@PathVariable(value="id") String id);

	@GetMapping("/products/customer/{custId}")
    List<Product> listProductsByCustomerId(@PathVariable(value="custId") String custId);
	
	@GetMapping("/products")
    Product create(@RequestBody Product product);

}
