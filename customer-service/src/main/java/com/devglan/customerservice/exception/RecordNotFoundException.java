package com.devglan.customerservice.exception;
/**
 * @author Marc Chester Lazatin
 *
 */
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -6626706914867260782L;
	private String exceptionDetail;
    private Object fieldValue;

    public RecordNotFoundException( String exceptionDetail, Long id) {
        super(exceptionDetail+" - "+id);
        this.exceptionDetail = exceptionDetail;
        this.fieldValue = id;
    }

    public RecordNotFoundException(String exceptionDetail2, String id) {
		// TODO Auto-generated constructor stub
    	super(exceptionDetail2+" - "+id);
        this.exceptionDetail = exceptionDetail2;
        this.fieldValue = id;
	}

	public String getExceptionDetail() {
        return exceptionDetail;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
}